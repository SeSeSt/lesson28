var allItemsSlider = document.querySelectorAll('.carousel > .carousel-inner > .carousel-item');
var controls = document.querySelector('.carousel > .controls');
var count = 0;

var next = document.querySelector('.carousel > .carousel-control-next');
var prev = document.querySelector('.carousel > .carousel-control-prev');
allItemsSlider[count].classList.add('active');

for (var i = 0; i < allItemsSlider.length; i++){
    var li = document.createElement('li');
    li.id = i;
    controls.appendChild(li);
    li.addEventListener('click', goToSlide)
}
controls.children[count].classList.add('active');

next.addEventListener('click', onNextSlide);
prev.addEventListener('click', onPrevSlide);
function onNextSlide() {
    allItemsSlider[count].classList.remove('active');
    controls.children[count].classList.remove('active');
    count++;
    if (count >= allItemsSlider.length){
        count = 0;
    }
    controls.children[count].classList.add('active');
    allItemsSlider[count].classList.add('active');
}

function onPrevSlide(){
    allItemsSlider[count].classList.remove('active');
    controls.children[count].classList.remove('active');
    if (count === 0){
        count = allItemsSlider.length;
    }
    count--;

    controls.children[count].classList.add('active');
    allItemsSlider[count].classList.add('active');
}

function  goToSlide(event) {
    var id = event.target.id;
    allItemsSlider[count].classList.remove('active');
    controls.children[count].classList.remove('active');
    count = +id;
    controls.children[id].classList.add('active');
    allItemsSlider[id].classList.add('active');
}






















